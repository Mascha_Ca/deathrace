﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {


	public Transform[] SpawnPoints;
	public float spawnTime = 1.5f;
	public int minHinderniss = 0, maxHinderniss =2;

	public GameObject [] Hinderniss;

	void Start () {
		InvokeRepeating ("SpawnItems", spawnTime, spawnTime);
	}
	void Update () {
	}

	void SpawnItems() 
	{ 
		int spawnIndex = Random.Range (minHinderniss, SpawnPoints.Length);
		int ObjectIndex = Random.Range (0, Hinderniss.Length);
		Instantiate (Hinderniss[ObjectIndex], SpawnPoints [spawnIndex].position, SpawnPoints [spawnIndex].rotation);	
	}

}
