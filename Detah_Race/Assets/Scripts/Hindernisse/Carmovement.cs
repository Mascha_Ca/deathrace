﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carmovement : MonoBehaviour {

	private int speedX = 0;
	private int speedY = 0;
	public int movingSpeed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


		transform.Translate ((new Vector3 (speedX, speedY, -1) * Time.deltaTime)*movingSpeed);
		
	}
}
