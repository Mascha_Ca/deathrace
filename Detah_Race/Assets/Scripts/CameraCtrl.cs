﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCtrl : MonoBehaviour {

	public Vector3 position; 

	public Vector2 constrainX; 

	public Vector2 constrainZ; 

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		position = this.transform.position; 

		position.y = 10; 

		position.x = Mathf.Clamp (this.transform.position.x, constrainX.x, constrainX.y); 

		position.z=  Mathf.Clamp (this.transform.position.z, constrainZ.x, constrainZ.y); 

		this.transform.position = position; 
	}
}
