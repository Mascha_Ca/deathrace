﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class PlayerController : MonoBehaviour {

	public float speed; 
	public Rigidbody myBODY; 

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.LeftArrow)) {

			myBODY.AddForce (Vector3.left * speed * Time.deltaTime); 
		}


		if (Input.GetKey (KeyCode.RightArrow)) {

			myBODY.AddForce ( Vector3.right * speed * Time.deltaTime); 

		}


		if (Input.GetKey (KeyCode.UpArrow)) {

			myBODY.AddForce (Vector3.forward * (speed + 1) * Time.deltaTime); 
		}

		if (Input.GetKey (KeyCode.DownArrow)) {

			myBODY.AddForce ( Vector3.back * speed * Time.deltaTime); 
		}
			
	}

	void OnCollisionEnter(Collision col){

		if (col.gameObject.tag == "Obstacle") {

			SceneManager.LoadScene ("EndScene");
			Destroy (this.gameObject); 

		}

	}
}
