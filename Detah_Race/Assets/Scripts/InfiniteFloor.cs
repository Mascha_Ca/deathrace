﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteFloor : MonoBehaviour {

	public GameObject [] floor; 

	public float spawnMin; 
	public float spawnMax; 

	// Use this for initialization
	void Start () {

		Spawn (); 
	}

	void Spawn(){

		Instantiate (floor [Random.Range (0, floor.GetLength (0))], transform.position, Quaternion.identity); 
		Invoke ("Spawn", Random.Range (spawnMax, spawnMin)); 

	}

	// Update is called once per frame
	void Update () {
		
	}
}
